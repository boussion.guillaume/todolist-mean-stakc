import { Component, OnInit } from '@angular/core';
import {TaskService} from '../../task.service';
import {ActivatedRoute, Router} from '@angular/router';
import {List} from '../../models/list.model';


@Component({
  selector: 'app-new-list',
  templateUrl: './new-list.component.html',
  styleUrls: ['./new-list.component.scss']
})
export class NewListComponent implements OnInit {

  constructor(private taskService: TaskService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
  }

  createNewList(title: string) {
    this.taskService.createList(title)
      .subscribe((list: any) => {
        // Retour à l'écran des listes avec la liste créée sélectionnée
        this.router.navigate(['/lists', list._id]);
      });
  }
}
