import { Component, OnInit } from '@angular/core';
import {TaskService} from '../../task.service';
import {ActivatedRoute, Params} from '@angular/router';
import {Task} from '../../models/task.model';
import {List} from '../../models/list.model';
import { TmplAstElement } from '@angular/compiler';

@Component({
  selector: 'app-task-view',
  templateUrl: './task-view.component.html',
  styleUrls: ['./task-view.component.scss']
})
export class TaskViewComponent implements OnInit {

  lists: List[];
  tasks: Task[];
  listId : string;

  constructor(private taskService: TaskService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        this.listId = params.listId;
        this.getTasks(this.listId);
      }
    );

    this.getLists();    
  }

  onTaskClick(task: Task) {
      // Passage de la Task en completed
      this.taskService.completeOrDecompleteTask(task).subscribe(() => {
        task.completed = !task.completed;        
      });    
  }

  getLists() {
    this.taskService.getLists()
      .subscribe((lists: List[]) => {
        this.lists = lists;
      });
  }

  getTasks(listId : string){
    this.taskService.getTasks(listId)
          .subscribe((tasks: Task[]) => {
            this.tasks = tasks;
          });
  }
}
