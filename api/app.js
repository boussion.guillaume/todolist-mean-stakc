const express = require('express');
const app = express();

const {mongoose} = require('./db/mongoose');
const cors = require('cors');

// const bodyParser = require('body-parser');

// Load mongoose models
const { List, Task } = require('./db/models');

// Charge body-parser, pour parser le JSON des réponses HTTP
app.use(express.json());
app.use(cors());

// CORS HEADERS MIDDLEWARE
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, HEAD, OPTIONS, PUT, PATCH, DELETE")
    next();
});

/* ROUTE HANDLERS */

/** LIST ROUTES */

/**
 * GET /lists
 * Purpose: Get all lists
 */
app.get('/lists', (req, res) => {
    // Retourne un Array de toutes les listes dans la base de données
    List.find({}).then((lists) => {
           res.send(lists);
        })
});

/**
 * POST /lists
 * Purpose: Create a list
 */
app.post('/lists', (req, res) => {
    // Ajoute une nouvelle liste and retourne le document de la nouvelle list (ID inclu)
    // Les infos (champs) seront envoyés via JSON (request)
    let title = req.body.title;

    let newList = new List({
        title
    });

    newList.save()
        .then((listDoc) => {
            // La liste est renvoyée en callback
            res.send(listDoc);
        })
});

/**
 *  UPDATE /lists/:id
 *  Purpose: Update a specific list
 */
app.patch('/lists/:id', (req, res) => {
    // MAJ d'une liste spécifique (document de la liste avec l'ID dans l'URL) avec les valeurs spécifiées dans le corps du JSON de la request
    List.findByIdAndUpdate({_id: req.params.id},
        {$set: req.body
        }).then(() => {
            res.sendStatus(200);
    });
});

/**
 * DELETE /lists/:id
 * Purpose: Delete a list
 */
app.delete('/lists/:id', (req, res) => {
    // Suppression d'une liste spécifique
    List.findByIdAndDelete({
        _id: req.params.id
    }).then((removedListDoc) => {
        res.send(removedListDoc);
    })
});

/**
 * GET Tasks of a List
 */
app.get('/lists/:listId/tasks', (req, res) =>  {
    Task.find({
        _listId: req.params.listId
    }).then((tasks) => {
        res.send(tasks);
    })
});

app.get('/lists/:listId/tasks/:taskId', (req, res) => {
    Task.findOne({
        _id: req.params.taskId,
        _listId: req.params.listId
    }).then((task) => {
        res.send(task);
    })
});

/**
 * POST Tasks in a List
 */
app.post('/lists/:listId/tasks', (req, res) => {
    let newTask = new Task({
        title: req.body.title,
        _listId: req.params.listId
    });

    newTask.save()
        .then((newTaskDoc) => {
            res.send(newTaskDoc);
        })
});

/**
 * PATCH /lists/:listId/Tasks/:taskId
 * Purpose: Update a specific task in a list
 */
app.patch('/lists/:listId/tasks/:taskId', (req, res) => {
    Task.findOneAndUpdate({
        _id: req.params.taskId,
        _listId: req.params.listId
    }, {
        $set: req.body
    }).then(() => {
        res.send({message: 'Update OK'});
    })
});


/**
 * POST /lists/:listId/Tasks/:taskId
 * Purpose: Update a specific task in a list
 */
 app.post('/lists/:listId/tasks/:taskId', (req, res) => {
    console.log(req.params);
    Task.findOneAndUpdate({
        _id: req.params.taskId,
        _listId: req.params.listId
    }, {
        $set: req.body
    }).then(() => {
        res.sendStatus(200);
    })
    .catch(() => {
        res.sendStatus(401);
    })
});

/**
 * DELETE /lists/:listId/Tasks/:taskId
 * Purpose: Delete a task
 */
app.delete('/lists/:listId/Tasks/:taskId', (req, res) => {
    Task.findOneAndRemove({
        _id: req.params.taskId,
        _listId: req.params.listId
    }).then((removedTask) => {
        res.send(removedTask);
    })
});

app.listen(3000, ()=>{
    console.log("Server is listening");
});
