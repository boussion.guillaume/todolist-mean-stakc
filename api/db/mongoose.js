// Ce fichier gère les logiques de connexion avec MongoDB

const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost:27017/TaskManager', { useNewUrlParser: true })
    .then (() => {console.log('Connexion réussie');
    }).catch ((e) => {
            console.log('Erreur lors de la tentative de connection');
            console.log(e);
});

// Pour éviter les erreurs de dépréciation
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

module.exports = {mongoose};
