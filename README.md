# TodoList - MEAN Stack

Courte application développée dans l'idée de monter en compétences sur les technologies Angular/NodeJS/Express/MongoDB. 
Cette application n'a fait l'objet d'aucun projet professionnel ou scolaire, elle était simplement un challenge personnel.

Mise en place de fonctionnalités CRUD (Create, Read, Update, Delete) sur une application de planification de tâches.
Le front-end est réalisé sous Angular (Bulma pour le styling), tandis que le back-end tourne sous NodeJS (avec le framework Express) et MongoDB comme base de données.

**Screenshot de l'application :**

![ALT](/Capture d’écran 2021-04-14 221330.png)
